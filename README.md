copied and modified from:
https://git.dthompson.us/guix-custom.git/blob/HEAD:/README.md

Manolis's Guix Overlay
=====================

A collection of custom Guix packages that aren't suitable for
submission upstream.

Usage
-----

Just point Guix towards the root of this source tree!

```
export GUIX_PACKAGE_PATH=/path/to/guix-custom
```

The packages in this repo will take precedence over those in the
official distribution.

