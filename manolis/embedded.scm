;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2017 Manolis Fragkiskos Ragkousis <manolis837@gmail.com>
;;;
;;; This file is an addendum to GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.


(define-module (manolis embedded)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  #:use-module ((gnu packages algebra) #:select (bc))
  #:use-module (gnu packages base)
  #:use-module (gnu packages bootloaders)
  #:use-module (gnu packages cross-base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages embedded)
  #:use-module (gnu packages python)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages swig))

;; gcc-linaro-arm-linux-gnueabihf-4.9-2014.09_linux.tar.xz
;; Ideally I want to rebuild this toolchain though Guix.
(define-public linaro-toolchain-4.9
  (package
    (name "linaro-toolchain")
    (version "4.9-2014.09_linux")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://releases.linaro.org/archive/14.09/components/\
toolchain/binaries/gcc-linaro-arm-linux-gnueabihf-" version ".tar.xz"))
       (sha256
        (base32
         "148q9xnwn8ygqzpxpv8ayj06cdnf27h4p8fzvn5krsx0mq6arzqc"))))
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
       #:builder (begin
                   (use-modules (guix build utils)
                                (srfi srfi-26))

                   (let* ((source (assoc-ref %build-inputs "source"))
                          (tar    (assoc-ref %build-inputs "tar"))
                          (xz     (assoc-ref %build-inputs "xz"))
                          (output (assoc-ref %outputs "out")))
                     (setenv "PATH" (string-append xz "/bin"))
                     (system* (string-append tar "/bin/tar") "xvf"
                              source)
                     (copy-recursively "gcc-linaro-arm-linux-gnueabihf-4.9-2014.09_linux/"
                                       output)
                     #t))))
    (native-inputs `(("tar" ,tar)
                     ("xz" ,xz)))
    (supported-systems '("i686-linux" "x86_64-linux"))
    (home-page "https://releases.linaro.org")
    (synopsis "Linaro arm cross-toolchain")
    (description
     "Linaro arm cross-toolchain binaries that can be used to build Fiasco, L4
 and L4Linux")
    (license license:gpl2)))

(define-public linaro-toolchain-6.4
  (package
    (name "linaro-toolchain")
    (version "6.4.1-2017.11-x86_64_arm-linux-gnueabihf")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://releases.linaro.org/components/toolchain/binaries/\
6.4-2017.11/arm-linux-gnueabihf/gcc-linaro-" version ".tar.xz"))
       (sha256
        (base32
         "1xln8if5xyzbrfa5bl8rny0ckj18xynz5hv8hsarws6rmrnxfn0p"))))
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
       #:builder (begin
                   (use-modules (guix build utils)
                                (srfi srfi-26))

                   (let* ((source (assoc-ref %build-inputs "source"))
                          (tar    (assoc-ref %build-inputs "tar"))
                          (xz     (assoc-ref %build-inputs "xz"))
                          (output (assoc-ref %outputs "out")))
                     (setenv "PATH" (string-append xz "/bin"))
                     (system* (string-append tar "/bin/tar") "xvf"
                              source)
                     (copy-recursively "gcc-linaro-6.4.1-2017.11-x86_64_arm-linux-gnueabihf/"
                                       output)
                     #t))))
    (native-inputs `(("tar" ,tar)
                     ("xz" ,xz)))
    (supported-systems '("i686-linux" "x86_64-linux"))
    (home-page "https://releases.linaro.org")
    (synopsis "Linaro arm cross-toolchain")
    (description
     "Linaro arm cross-toolchain binaries that can be used to build U-boot and Linux for SAMA5D3")
    (license license:gpl2)))

(define-public free-toolchain-4.9
  (package
    (name "free-toolchain")
    (version "4.9-2014.09_linux")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://releases.linaro.org/archive/14.09/components/\
toolchain/binaries/gcc-linaro-arm-linux-gnueabihf-" version ".tar.xz"))
       (sha256
        (base32
         "148q9xnwn8ygqzpxpv8ayj06cdnf27h4p8fzvn5krsx0mq6arzqc"))))
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
       #:builder (begin
                   (use-modules (guix build utils)
                                (srfi srfi-26))

                   (let* ((source (assoc-ref %build-inputs "source"))
                          (tar    (assoc-ref %build-inputs "tar"))
                          (xz     (assoc-ref %build-inputs "xz"))
                          (output (assoc-ref %outputs "out")))
                     (setenv "PATH" (string-append xz "/bin"))
                     (system* (string-append tar "/bin/tar") "xvf"
                              source)
                     (copy-recursively "gcc-linaro-arm-linux-gnueabihf-4.9-2014.09_linux/"
                                       output)
                     #t))))
    (native-inputs `(("tar" ,tar)
                     ("xz" ,xz)))
    (supported-systems '("i686-linux" "x86_64-linux"))
    (home-page "https://releases.linaro.org")
    (synopsis "Linaro arm cross-toolchain")
    (description
     "Linaro arm cross-toolchain binaries that can be used to build Fiasco, L4
 and L4Linux")
    (license license:gpl2)))


;; http://toolchains.free-electrons.com/downloads/releases/toolchains/armv7-eabihf/tarballs/armv7-eabihf--uclibc--stable-2017.05-toolchains-1-1.tar.bz2

(define u-boot
  (package
    (name "u-boot")
    (version "2018.01")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "ftp://ftp.denx.de/pub/u-boot/"
                    "u-boot-" version ".tar.bz2"))
              (sha256
               (base32
                "1nidnnjprgxdhiiz7gmaj8cgcf52l5gbv64cmzjq4gmkjirmk3wk"))))
    (native-inputs
     `(("bc" ,bc)
       ;("dtc" ,dtc) ; they have their own incompatible copy.
       ("python-2" ,python-2)
       ("swig" ,swig)))
    (build-system  gnu-build-system)
    (home-page "http://www.denx.de/wiki/U-Boot/")
    (synopsis "ARM bootloader")
    (description "U-Boot is a bootloader used mostly for ARM boards. It
also initializes the boards (RAM etc).")
    (license license:gpl2+)))

(define-public u-boot-tools
  (package (inherit u-boot)
    (name "u-boot-tools")
    (native-inputs
     `(("cross-gcc" ,(cross-gcc "arm-linux-gnueabihf"))
       ("cross-binutils" ,(cross-binutils "arm-linux-gnueabihf"))
       ("sdl" ,sdl)
       ("openssl" ,openssl)
       ,@(package-native-inputs u-boot)))
    (arguments
     `(#:modules ((ice-9 ftw) (guix build utils) (guix build gnu-build-system))
       #:make-flags (list "HOSTCC=gcc" )
       #:phases
       (modify-phases %standard-phases
         (delete 'check)
         (replace 'configure
           (lambda* (#:key make-flags #:allow-other-keys)
             (zero? (apply system* "make" `(,@make-flags "defconfig")))))
         (replace 'build
           (lambda* (#:key make-flags #:allow-other-keys)
             (zero? (apply system* "make" `(,@make-flags "tools-all")))))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out")))
               (mkdir-p (string-append out "/bin"))
               (for-each (lambda (file)
                           (install-file (string-append "tools/" file) (string-append out "/bin")))
                         '("bmp_logo" "dumpimage" "fdtgrep" "fit_check_sign"
                           "fit_info" "genboardscfg.py" "gen_eth_addr" "gen_ethaddr_crc"
                           "img2srec" "jtagconsole" "microcode-tool.py" "mkenvimage"
                           "mkimage" "mksunxiboot" "moveconfig.py" "ncb"
                           "netconsole" "proftool" "rkmux.py" "ubsha1" "xway-swap-bytes")))
             #t)))))))

(define-public l4fiasco-env
  (package
    (name "l4fiasco-env")
    (version "0.1")
    (source #f)
    (build-system trivial-build-system)
    (native-inputs
     `(("linaro-toolchain" ,linaro-toolchain-4.9)
       ("u-boot" ,u-boot-tools)
       ("dtc" ,dtc)
       ("doxygen" ,doxygen)))
    (synopsis "Octopress Ruby Environment")
    (description "This file automates the creation of a cross-arm environment so I can
build Fiasco,L4 and L4Linux.")
    (home-page #f)
    (license license:expat)))

(define-public sama5d3-env
  (package
   (name "sama5d3-env")
   (version "0.1")
   (source #f)
   (build-system trivial-build-system)
   (native-inputs
    `(("linaro-toolchain" ,linaro-toolchain-6.4)
      ("u-boot" ,u-boot-tools)
      ("dtc" ,dtc)
      ("doxygen" ,doxygen)))
   (synopsis "ATMEL SAMA5D3 ENVIRONMENT")
   (description "This file automates the creation of a cross-arm environment so I can
build u-boot and linux for atmel sama5d3.")
   (home-page #f)
   (license license:expat)))
